import { HerokuAppPage } from './app.po';

describe('heroku-app App', function() {
  let page: HerokuAppPage;

  beforeEach(() => {
    page = new HerokuAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
